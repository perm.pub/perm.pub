#!/usr/bin/python3

import logging, os
from pathlib import Path

import jinjagen
import tomli

from epijats import Webstract
from hidos.dsi import BaseDsi
from hidos.cache import SuccessionCache
from hidos.util import LOG


WEBSRC = Path("websrc")
BUILD = Path("_build")
with open("config.toml", "rb") as f:
    CONFIG = tomli.load(f)
SUCC_CACHE = Path("_succession_cache")
SWHA_BEARER_TOKEN_PATH = Path("swh-token.txt")

assert WEBSRC.is_dir()


def render_succession(succession, gen, tmp, eprinters):
    editions = succession.root.all_subeditions() + [succession.root]
    docs = dict()
    for e in editions:
        if e.snapshot:
            ws = Webstract.from_edition(e, Path(tmp) / str(e.dsi))
            docs[e.edid] = ws.facade
    for e in editions:
        doc_edition = e.flow_edition()
        ctx = dict(
            path_edid=e.edid,
            succession=succession,
            doc=docs[doc_edition.edid] if doc_edition else None,
            all_docs=docs,
            eprinters=eprinters,
        )
        subpath = Path(succession.dsi.base64) / str(e.edid)
        gen.gen_file("_edition.html.jinja", subpath / "index.html", ctx)


def make_website(dest_dir, offline: bool):
    if SWHA_BEARER_TOKEN_PATH.exists():
        with open(SWHA_BEARER_TOKEN_PATH) as f:
            os.environ["SWHA_BEARER_TOKEN"] = f.read().strip()
    cache = SuccessionCache(SUCC_CACHE, offline=offline)
    signed_dsi = set()
    gen = jinjagen.JinjaGenerator(WEBSRC, dest_dir)
    for key, subconfig in CONFIG["successions"].items():
        dsi = BaseDsi(key)
        print(dsi)
        ds = cache.get(dsi)
        if ds.is_signed:
            signed_dsi.add(dsi)
        eprinters = {}
        for eprinter in subconfig.get('eprinters', []):
            eprinters[eprinter] = CONFIG["eprinters"][eprinter]
        render_succession(ds, gen, BUILD, eprinters)

    gen.env.globals.update(dict(dsis=sorted(signed_dsi)))
    gen.gen_site()


if __name__ == "__main__":
    import argparse

    os.environ["EPIJATS_NO_PANDOC"] = "1"

    LOG.setLevel(logging.INFO)
    LOG.addHandler(logging.StreamHandler())

    parser = argparse.ArgumentParser(description="Generate website")
    parser.add_argument("output", help="Path to ouput directory")
    parser.add_argument(
        "--offline", action="store_true", help="Make offline, use cache only"
    )
    args = parser.parse_args()
    make_website(Path(args.output), offline=args.offline)
